db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "red",
		"stock": 20,
		"price": 40,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["Philippines", "USA"]
	},
	{
		"name": "Banana",
		"color": "yellow",
		"stock": 15,
		"price": 20,
		"supplier_id": 2,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "green",
		"stock": 25,
		"price": 50,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["China", "USA"]
	},
	{
		"name": "Mango",
		"color": "yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}

]);

//2
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}
]);

//3
db.fruits.aggregate([
		{$match: {"stock": {$gt: 20}}},
		{$count: "enoughStock"}
]);

//4
db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);

//5
db.fruits.aggregate([
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
]);

//6
db.fruits.aggregate([
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
]);

